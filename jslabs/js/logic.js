function absValue(value){

    let reverseValue = 'введено не число';

    if (!isNaN(value)){
        if (value >= 0) {
            console.log(value);
        } else if (value < 0) {
            reverseValue = value * -1;
        }
    }

    return reverseValue;
}

function isPalindrome(str) {

    let len = str.length;
    let mid = Math.floor(len/2);

    for ( let i = 0; i < mid; i++ ) {
        if (str[i] !== str[len - 1 - i]) {
            return false;
        }
    }

    return true;
}

function matrixMatch(array){
    let resultArr = [];
    array.forEach(element => {
        element.forEach(element => {
            resultArr.push(element);
        })
    });

    result = (resultArr.every((val, i, arr) => val.length === arr[0].length));

    return result;
}

function matrixAddition(A,B) { 
    
    array = [];
    array.push(A, B);
    if(matrixMatch(array)){
        if(A.length == B.length){
            let m = A.length, n = A[0].length, C = [];
            for (let i = 0; i < m; i++){
                C[i] = [];
                for (let j = 0; j < n; j++) C[i][j] = A[i][j]+B[i][j];
            }
            let ar1 = C[0];
            let ar2 = C[1];
            console.log(ar1.join('  '));
            console.log(ar2.join('  '));
        } else {
            console.log('Операция невыполнима');
        }
    } else {
        console.log('Операция невыполнима');
    }

}

function showProps(obj){
    let props = Object.keys(obj).join(", ");
    console.log(`Список свойств: ${props}`)
}

function showObj(obj){
    console.log(`Студент ${obj.first_name} ${obj.last_name} учится в ${obj.group} группе`)
}