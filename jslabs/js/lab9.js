typeof(9)
// Предположение: 'number'
// Фактический: 'number'

typeof(1.2)
// Предположение: 'number'
// Фактический: 'number'

typeof(NaN)
// Предположение: 'number'
// Фактический: 'number'

typeof("Hello World")
// Предположение: 'string'
// Фактический: 'string'

typeof(true)
// Предположение: 'boolean'
// Фактический: 'boolean'

typeof(2 != 1)
// Предположение: 'boolean'
// Фактический: 'boolean'


"сыр" + "ы"
// Предположение: 'сыры'
// Фактический: 'сыры'

"сыр" - "ы"
// Предположение: NaN
// Фактический: NaN

"2" + "4"
// Предположение: '24'
// Фактический: '24'

"2" - "4"
// Предположение: '-2'
// Фактический: '-2'

"Сэм" + 5
// Предположение: 'Сэм5'
// Фактический: 'Сэм5'

"Сэм" - 5
// Предположение: NaN
// Фактический: Nan

99 * "шары"
// Предположение: NaN
// Фактический: NaN

function per(height, width){
    console.log(height + ' ' + width);
    let perim = 2 * (height + width);
    console.log(perim);
    let area = height * width;
    console.log(area);
    console.log(perim / area);
}

per(2,3)

let celsius = 25;
let fahrenheit = 66.2;
let celsiusToFahrenheit = ((celsius * 9/5) + 32);
let fahrenheitToCelsius = ((fahrenheit - 32) * 5/9);
console.log(`${celsius}°C соответствует ${celsiusToFahrenheit}°C`);
console.log(`${fahrenheit}°F соответствует ${fahrenheitToCelsius}°C`);

function yearMath(){
    let year = prompt('Введите год');

    let isLeap = year%4 == 0 ? (year%100 !== 0 ? true : false) : false

    alert(isLeap);
}

function isTenFunc(key, value){
    let isTen = false;

    if (key == 10 || value == 10){
        isTen = true;
    } else if (key + value == 10){
        isTen = true;
    }

    console.log(isTen);
}

function sheepCounter(){
    let sheepString = '';
    let sheep = prompt('Введите число овец');
    for (i=1; i<=sheep; i++){
        sheepString += i + ' овечка...';
    }
    console.log(sheep)
    console.log(sheepString)
}

function numberCounter(){
    for (i=0; i<=15; i++){
        isEven = ' нечетное';
        if (i%2 == 0){
            isEven = ' четное' 
        }
        console.log('"' + i + isEven + '"');
    }
}

function tree(){
    for (i=1; i<=10; i++){
        if(i%2==0){
            str = '#';
        } else {
            str = '*';
        }
        console.log(str.repeat(i));
    }
}

function sortFunc(a, b, c) {
    array = [];
    array.push(a, b, c);
    let temp = 0;
    for (let i = 0; i < array.length; i++) {
        for (let j = i; j < array.length; j++) {
            if (array[j] < array[i]) {
                temp = array[j];
                array[j] = array[i];
                array[i] = temp;
            }
        }
    }
    console.log(array.join(', '));
}

function maxFunc(a, b, c, d, e){
    let maxN = a

    if (b>maxN){
        maxN = b
    } else if (c>maxN){
        maxN = c
    } else if (d>maxN){
        maxN = d;
    } else if(e>maxN){
        maxN = e;
    }

    console.log(maxN);
}

